print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 2 del capitulo 3. Autora: Lilia Susana Tene')

#Ejercicio 2: Reescribe el programa del salario usando try y except, de modo que el
#programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando
#un mensaje y saliendo del programa. A continuación se muestran dos ejecuciones
#del programa:

try:
    print("Introduzca las Horas:")
    horas = float(input())
    print("Introduzca la Tarifa por hora:")
    tarifa = float(input())
    if horas  <= 40:
        salario = (horas * tarifa)
    if  horas > 40:
        hextras = horas - 40
        salario = (40 * tarifa) + (hextras * 1.5 * tarifa)
        print("Su salario es:", salario)
except:
    print('Error, por favor introduzca un número')