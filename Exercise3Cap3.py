print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 3 del capitulo 3. Autora: Lilia Susana Tene')

#Escribe un programa que solicite una puntuación entre 0.0 y 1.0.
# Si la puntuación está fuera de ese rango, muestra un mensaje de error.
# Si la puntuación está entre 0.0 y 1.0, muestra la calificación usando
# la tabla siguiente:

try:
    puntuacion = float(input('Introduzca puntuacion='))
    # validacion del rango de la puntuacion
    if puntuacion >= 0 and puntuacion <=1.0:
        if puntuacion >=0.9:
            print('Sobresaliente')
        elif puntuacion >=0.8:
            print('Notable')
        elif puntuacion >= 0.7:
            print('Bien')
        elif puntuacion >= 0.6:
            print('Suficiente')
        elif puntuacion < 0.6:
            print('Insuficiente')
    else:
        print('puntuacion incorrecta')
except:
    print('Puntuacion Incorrecta')
